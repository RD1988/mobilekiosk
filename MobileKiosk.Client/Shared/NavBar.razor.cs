﻿using Microsoft.AspNetCore.Components;
using MobileKiosk.Client.Models;
using MobileKioskClient.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileKiosk.Client.Shared
{
    public partial class NavBar
    {

        [Inject]
        public IProductHttpRepository ProductRepo { get; set; }

        public ApplicationUser applicationUser { get; set; }


        [Inject]
        public Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }


        public string restaurantName { get; set; }

        protected async override Task OnInitializedAsync()
        {
            await GetAccessToken();

            await GetRestaurant(applicationUser.token);
        }

        private async Task GetAccessToken()
        {


            applicationUser = await ProductRepo.GetAccessToken();


            await localStorage.SetItemAsync("token", applicationUser.token);


        }

        private async Task GetRestaurant(string token)
        {
      

            token = await localStorage.GetItemAsync<string>("token");


            applicationUser = await ProductRepo.GetRestaurant(token);

            restaurantName = applicationUser.restaurant.restaurantName;

        

        }

     
    }
}
