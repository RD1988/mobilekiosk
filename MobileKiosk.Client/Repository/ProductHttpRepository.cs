﻿
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Http;
using Microsoft.AspNetCore.WebUtilities;
using MobileKiosk.Client.Models;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MobileKioskClient.Repository
{
    public class ProductHttpRepository : IProductHttpRepository
    {
        private readonly HttpClient _client;
        private HttpRequestMessage _request;
        private readonly JsonSerializerOptions _options;

        [Inject]
        public NavigationManager navigationManager { get; set; }

        public Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }


        public ProductHttpRepository(HttpClient client)
        {


            _client = client;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };


            _request = new HttpRequestMessage
            {

                RequestUri = new Uri("https://webapidevelopmentemploydk.azurewebsites.net/"),

            };

           
        }

        public async Task<ApplicationUser> GetAccessToken()
        {
         

            var values = new Dictionary<string, string>
            {
            { "username", "pitahouseranders" },
            { "password", "123" }
            };

          
            string url = "account/login";
            var data = new FormUrlEncodedContent(values);
        
            var response = await _client.PostAsync(_request.RequestUri + url, data);

         

            if (response.IsSuccessStatusCode)
            {
                var body = await response.Content.ReadAsStringAsync();

              
                ApplicationUser user = new ApplicationUser()
                {

                    token = body

                };

           
               return user;

              
            }



            return null;
        }

        public async Task<ApplicationUser> GetRestaurant(string token)
        {

            var url = "Kiosk/GetUserAsync";

            _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var response = await _client.GetStringAsync(_request.RequestUri + url);


            ApplicationUser applicationUser = System.Text.Json.JsonSerializer.Deserialize<ApplicationUser>(response);


          



            return applicationUser;
        }

        public async Task<List<Root>> GetMenuItems()
        {
            var url = "Kiosk/GetMenuItemsAsync";

            _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwaXRhaG91c2VyYW5kZXJzIiwianRpIjoiYjEzOWI4NTItZTJmYi00NGE5LTk0YTAtMjVhYzNiODBiN2IyIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZWlkZW50aWZpZXIiOiI0ZDY1NDhmNS01NDYzLTRjZGEtOWZhZS0yNjEwYTk3YmM3YmEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9kYXRlb2ZiaXJ0aCI6IjgvMTkvMjAyMSAzOjU2OjI5IFBNIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiRnJvbnREZXNrIiwiZXhwIjoxNjQyMzQ4NjQ5LCJpc3MiOiJDYXNoaWVySXNzdWVyIiwiYXVkIjoiQ2FzaGllckF1ZGllbmNlIn0.3F - AXlbH2m_ - zWf3oK1xmiE6iu4TU1YExzQ1W - Z15OY");

            var response = await _client.GetStringAsync(_request.RequestUri + url);




            Console.WriteLine(response);
            return null;
        }


        //public async Task GetProductRelations(string id)
        //{
        //    var url = Path.Combine(id.ToString());


        //   // Console.WriteLine("Hello from Repo, you have clicked on the following id.." + url);

        //}


    }
}
