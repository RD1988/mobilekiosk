﻿using MobileKiosk.Client.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileKioskClient.Repository
{
    public interface IProductHttpRepository
    {
       
 

        Task<ApplicationUser> GetAccessToken();

        Task<ApplicationUser> GetRestaurant(string token);

        Task<List<Root>> GetMenuItems();

        

        //Task GetProductRelations(string id);



    }
}
