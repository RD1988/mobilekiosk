﻿using Microsoft.AspNetCore.Components;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileKiosk.Client.Components
{
    public partial class ProductTable
    {

        List<string> list = new List<string>();



        //this callback will send the value back to the parent component.
        
        [Parameter]
        public EventCallback<string> OnClick { get; set; }

        [Inject]
        public Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }

        private  string Clicked(string id)
        {

            
        

            return OnClick.InvokeAsync(id).ToString();
      


        }

    

    
    }
}
