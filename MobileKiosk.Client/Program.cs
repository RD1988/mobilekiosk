using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MobileKioskClient.Repository;
using MudBlazor.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MobileKiosk.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            //
            //builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("https://kiosk.renedahl.dk/") });
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddScoped<IProductHttpRepository, ProductHttpRepository>();

            builder.Services.AddMudServices();

            builder.Services.AddBlazoredLocalStorage();

          
            await builder.Build().RunAsync();
        }
    }
}
