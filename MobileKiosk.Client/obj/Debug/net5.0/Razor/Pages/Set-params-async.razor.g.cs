#pragma checksum "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Pages\Set-params-async.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b64288d1baa31fe4321953f3e2105f4db5d0f17a"
// <auto-generated/>
#pragma warning disable 1591
namespace MobileKiosk.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MobileKiosk.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MobileKiosk.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MobileKiosk.Client.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MudBlazor;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/set-params-async/{Param?}")]
    public partial class Set_params_async : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "p");
            __builder.AddContent(1, 
#nullable restore
#line 3 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Pages\Set-params-async.razor"
    message

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 5 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Pages\Set-params-async.razor"
       
    private string message = "Not set";

    [Parameter]
    public string Param { get; set; }

    public override async Task SetParametersAsync(ParameterView parameters)
    {
        if (parameters.TryGetValue<string>(nameof(Param), out var value))
        {
            if (value is null)
            {
                message = "The value of 'Param' is null.";
            }
            else
            {
                message = $"The value of 'Param' is {value}.";
            }
        }

        await base.SetParametersAsync(parameters);
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
