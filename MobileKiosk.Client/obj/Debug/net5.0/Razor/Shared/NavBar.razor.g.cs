#pragma checksum "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1b1b796120c67ec493da6e5a1c0853da2d225279"
// <auto-generated/>
#pragma warning disable 1591
namespace MobileKiosk.Client.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MobileKiosk.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MobileKiosk.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MobileKiosk.Client.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\_Imports.razor"
using MudBlazor;

#line default
#line hidden
#nullable disable
    public partial class NavBar : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<MudBlazor.MudAppBar>(0);
            __builder.AddAttribute(1, "Color", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Color>(
#nullable restore
#line 1 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                  Color.Success

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(2, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<MudBlazor.MudIconButton>(3);
                __builder2.AddAttribute(4, "Icon", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 2 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                          Icons.Material.Filled.Person

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(5, "Color", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Color>(
#nullable restore
#line 2 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                                                               Color.Inherit

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(6, "Edge", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Edge>(
#nullable restore
#line 2 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                                                                                    Edge.Start

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(7, "OnClick", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 2 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                                                                                                           (e) => DrawerToggle()

#line default
#line hidden
#nullable disable
                )));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(8, "\r\n    ");
                __builder2.OpenComponent<MudBlazor.MudSpacer>(9);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(10, "\r\n    ");
                __builder2.OpenComponent<MudBlazor.MudText>(11);
                __builder2.AddAttribute(12, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.AddContent(13, 
#nullable restore
#line 6 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
         restaurantName

#line default
#line hidden
#nullable disable
                    );
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(14, "\r\n    \r\n\r\n    ");
                __builder2.OpenComponent<MudBlazor.MudSpacer>(15);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(16, "\r\n\r\n    ");
                __builder2.OpenComponent<MudBlazor.MudBadge>(17);
                __builder2.AddAttribute(18, "Content", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 12 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                       0

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(19, "Color", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Color>(
#nullable restore
#line 12 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                                 Color.Error

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(20, "Overlap", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 12 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                                                       true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(21, "Class", "mx-6 my-4");
                __builder2.AddAttribute(22, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.OpenComponent<MudBlazor.MudIcon>(23);
                    __builder3.AddAttribute(24, "Icon", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 13 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                        Icons.Material.Filled.ShoppingCart

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(25, "Color", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Color>(
#nullable restore
#line 13 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                                                                   Color.Inherit

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(26, "\r\n");
            __builder.OpenComponent<MudBlazor.MudDrawer>(27);
            __builder.AddAttribute(28, "Open", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 18 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
                        _drawerOpen

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(29, "OpenChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.Boolean>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.Boolean>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _drawerOpen = __value, _drawerOpen))));
            __builder.AddAttribute(30, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<MobileKiosk.Client.Shared.NavMenu>(31);
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(32, "\r\n\r\n\r\n");
            __builder.AddMarkupContent(33, "<style>\r\n    .mdc-top-app-bar {\r\n        background-color: red;\r\n        color: gold;\r\n    }\r\n</style>");
        }
        #pragma warning restore 1998
#nullable restore
#line 22 "C:\Users\Rene\Desktop\mobilekiosk\MobileKiosk.Client\Shared\NavBar.razor"
        bool _drawerOpen = false;

    void DrawerToggle()
    {
        _drawerOpen = !_drawerOpen;
    } 





#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
