﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileKiosk.Client.Models
{
    [Serializable]
    public class ApplicationUser
    {

        public ApplicationUser()
        {
            //relations to video and images here..
        }

        public string userName { get; set; }
        public string passWord { get; set; }

        public string token { get; set; }


        //Make relations to restaurant  

        public Restaurant restaurant { get; set; }

     
    }
}
