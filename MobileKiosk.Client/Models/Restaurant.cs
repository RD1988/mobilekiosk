﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileKiosk.Client.Models
{

    public class Restaurant
    {
        public Restaurant()
        {
           // catalogs = new List<Catalog>();
        }

        public string id;
        public List<Restaurant> restaurants { get; set; }

        public string restaurantName { get; set; }

        public string city { get; set; }

        public string zipCode { get; set; }

        public string address { get; set; }

        public string phoneNo { get; set; }

        public string vat { get; set; }

        //public Image image;

        public string merchanT_IDENTIFIER;

        public string merchanT_SECRET;

        //Restaurant can have different catalogs, different ways of showing products, etc.
        //public List<Catalog> catalogs;

        //Possible methods this restaurant offers to pay
        //public PaymentOptions[] paymentOptions;

        //Front desk order options: Eat here or take away?
       // public OrderOptionsFrontDesk[] orderOptionsFrontDesk;


        // -- Options -- //

        //If true: OrderOptionsFrontDesk tells us if order is "eat in" or "take away"
        //If false: OrderOptionsFrontDesk is not taken into account. There is no options for "eat in" or "take away"
        public bool useOrderOptions;

        //If true, the customer is asked to enter their table number during order creation. Customer will only be promted
        //for table number if "EatHere" is choosen. Don't ask for table number when "TakeAway"
        public bool useTableNumber;

        //If true, force user to enter table number. If false, have a "skip" button for table number
        public bool forceUseTableNumber;

        // -- Options -- //

        public bool sendTransactionInfoToTillty;
    }
}
